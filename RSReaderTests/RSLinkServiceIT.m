//
//  Created by ajjnix on 20/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSTestCaseIT.h"
#import "RSLinkService.h"
#import <OCMock/OCMock.h>
#import <MagicalRecord/MagicalRecord.h>


@interface RSLinkServiceIT : RSTestCaseIT

@property (strong, nonatomic) RSLinkService *service;
@property (strong, nonatomic) id mockUserDefaults;

@end


@implementation RSLinkServiceIT

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    [MagicalRecord setupCoreDataStackWithInMemoryStore];
    self.service = [RSLinkService sharedInstance];
    
    id userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:RSHasBeenAddStandardLink];
    self.mockUserDefaults = OCMPartialMock(userDefaults);
}

- (void)tearDown {
    [MagicalRecord cleanUp];
    [self.mockUserDefaults stopMocking];
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
}

#pragma mark test
- (void)testOnFirstRunHave2Link {
    OCMStub([self.mockUserDefaults boolForKey:RSHasBeenAddStandardLink]).andReturn(NO);
    
    [self asyncTest:^(XCTestExpectation *expectation) {
        @weakify(self);
        [self.service list:^(NSArray *items) {
            @strongify(self);
            [expectation fulfill];
            XCTAssertEqual([items count], 2);
        } failure:^{
            @strongify(self);
            [expectation fulfill];
            XCTFail(@"error");
        }];
    } timeout:0.1];
}

#pragma mark test
- (void)testList {
    [self asyncTest:^(XCTestExpectation *expectation) {
        [self asyncTestList:expectation];
    } timeout:0.1];
}

- (void)asyncTestList:(XCTestExpectation *)expectation {
    NSString *rss1 = @"http://news.rambler.ru/rss/scitech1/";
    NSString *rss2 = @"http://news.rambler.ru/rss/scitech2/";
    
    RACSignal *signalAdd1 = [self createSignalAddRSS:rss1];
    RACSignal *signalAdd2 = [self createSignalAddRSS:rss2];
    RACSignal *signalRemove = [self createSignalRemove:rss1];
    RACSignal *signalList = [self createSignalList];
    
    [[[[[signalAdd1 flattenMap:^RACStream *(id _) {
        return signalAdd2;
    }] flattenMap:^RACStream *(id _) {
        return signalRemove;
    }] flattenMap:^RACStream *(id _) {
        return signalList;
    }] finally:^{
        [expectation fulfill];
    }] subscribeNext:^(NSArray *items) {
        XCTAssertEqual([items count], 1);
        XCTAssertEqualObjects(items[0], rss2);
    } error:^(NSError *error) {
        XCTFail(@"%@", error);
    }];
}

- (RACSignal *)createSignalAddRSS:(NSString *)rss {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        [self.service add:rss success:^{
            [subscriber sendNext:nil];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            @strongify(self);
            XCTFail(@"%@", error);
        }];
        return nil;
    }];
}

- (RACSignal *)createSignalRemove:(NSString *)rss {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        [self.service remove:rss success:^{
            [subscriber sendNext:nil];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            @strongify(self);
            XCTFail(@"%@", error);
        }];
        return nil;
    }];
}

- (RACSignal *)createSignalList {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        [self.service list:^(NSArray *items) {
            [subscriber sendNext:items];
            [subscriber sendCompleted];
        } failure:^{
            [subscriber sendError:nil];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
}

@end
