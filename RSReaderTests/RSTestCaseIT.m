//
//  Created by ajjnix on 28/12/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "RSTestCaseIT.h"
#import <OHHTTPStubs/OHHTTPStubs.h>


@interface RSTestCaseIT ()
@end


@implementation RSTestCaseIT

- (void)tearDown {
    [OHHTTPStubs removeAllStubs];
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    [super tearDown];
}

- (void)stubHttpErrorDomain:(NSString *)domain code:(NSInteger)code userInfo:(NSDictionary *)userInfo {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return YES;
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        NSError *error = [NSError errorWithDomain:domain code:code userInfo:userInfo];
        return [OHHTTPStubsResponse responseWithError:error];
    }];
}


@end
