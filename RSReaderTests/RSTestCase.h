//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <XCTest/XCTest.h>


typedef void (^RSTestCaseAsync)(XCTestExpectation *expectation);


@interface RSTestCase : XCTestCase

- (void)asyncTest:(RSTestCaseAsync)async;
- (void)asyncTest:(RSTestCaseAsync)async timeout:(NSTimeInterval)timeout;

@end
