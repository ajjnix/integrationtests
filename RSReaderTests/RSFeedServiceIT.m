//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSTestCaseIT.h"
#import "RSFeedService.h"
#import <OHHTTPStubs/OHHTTPStubs.h>
#import <OHHTTPStubs/OHPathHelpers.h>


@interface RSFeedServiceIT : RSTestCaseIT
@property (strong, nonatomic) RSFeedService *service;
@property (strong, nonatomic) NSString *url;

@end


@implementation RSFeedServiceIT

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.service = [RSFeedService sharedInstance];
    self.url = @"http://images.apple.com/main/rss/hotnews/hotnews.rss";
}

#pragma mark test
- (void)testFeedFromURL {
    [self stubXmlFeed];
    
    [self asyncTest:^(XCTestExpectation *expectation) {
        @weakify(self);
        [self.service feedFromStringUrl:self.url success:^(NSArray *itemNews) {
            @strongify(self);
            [expectation fulfill];
            XCTAssertNotNil(itemNews);
            XCTAssertEqual([itemNews count], 20);
        } failure:^(NSError *error) {
            @strongify(self);
            [expectation fulfill];
            XCTFail(@"%@", error);
        }];
    }];
}

- (void)stubXmlFeed {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return YES;
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        NSString *xmlFeed = OHPathForFile(@"rss_news.xml", [self class]);
        NSDictionary *headers = @{
                                  @"Content-Type" : @"application/xml"
                                  };
        return [OHHTTPStubsResponse responseWithFileAtPath:xmlFeed statusCode:200 headers:headers];
    }];
}

#pragma mark test
- (void)testFeedFromURLErrorInternet {
    [self stubHttpErrorDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
    
    [self asyncTest:^(XCTestExpectation *expectation) {
        @weakify(self);
        [self.service feedFromStringUrl:self.url success:^(NSArray *itemNews) {
            @strongify(self);
            [expectation fulfill];
            XCTFail(@"this is error");
        } failure:^(NSError *error) {
            @strongify(self);
            [expectation fulfill];
            XCTAssertEqualObjects([error domain], NSURLErrorDomain);
            XCTAssertEqual([error code], NSURLErrorNotConnectedToInternet);
        }];
    }];
}

#pragma mark test
- (void)testFeedFromURLErrorServerNotFound {
    [self stubHttpErrorDomain:NSURLErrorDomain code:NSURLErrorCannotFindHost userInfo:nil];
    
    [self asyncTest:^(XCTestExpectation *expectation) {
        @weakify(self);
        [self.service feedFromStringUrl:self.url success:^(NSArray *itemNews) {
            @strongify(self);
            [expectation fulfill];
            XCTFail(@"this is error");
        } failure:^(NSError *error) {
            @strongify(self);
            [expectation fulfill];
            XCTAssertEqualObjects([error domain], NSURLErrorDomain);
            XCTAssertEqual([error code], NSURLErrorCannotFindHost);
        }];
    }];
}

@end
