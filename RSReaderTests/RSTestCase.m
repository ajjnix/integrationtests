//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSTestCase.h"


@interface RSTestCase ()
@end


@implementation RSTestCase

- (void)testInitAfterSetUp {
    XCTAssert(YES, @"instance create");
}

- (void)asyncTest:(RSTestCaseAsync)async {
    [self asyncTest:async timeout:5.0];
}

- (void)asyncTest:(RSTestCaseAsync)async timeout:(NSTimeInterval)timeout {
    XCTestExpectation *expectation = [self expectationWithDescription:@"block not call"];
    XCTAssertNotNil(async, @"don't send async block!");
    async(expectation);
    [self waitForExpectationsWithTimeout:timeout handler:nil];
}

@end
