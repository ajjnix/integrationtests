//
//  Created by ajjnix on 28/12/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "RSTestCase.h"


@interface RSTestCaseIT : RSTestCase

- (void)stubHttpErrorDomain:(NSString *)domain code:(NSInteger)code userInfo:(NSDictionary *)userInfo;

@end
