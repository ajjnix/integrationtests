//
//  Created by ajjnix on 06/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RSLinkEntity : NSManagedObject

@property (nonatomic, retain) NSString *link;

@end
