//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RSFeedParser : NSObject

+ (instancetype)sharedInstance;
- (NSArray *)itemFeed:(NSDictionary *)dom;

@end
