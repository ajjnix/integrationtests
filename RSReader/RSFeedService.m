//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSFeedService.h"
#import "RSFeedParser.h"
#import <AFNetworking/AFNetworking.h>
#import <XMLDictionary/XMLDictionary.h>


@interface RSFeedService ()

@property (strong, nonatomic) RSFeedParser *parser;
@property (strong, nonatomic) AFHTTPRequestOperationManager *transportLayer;

@end


@implementation RSFeedService

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static RSFeedService *instance;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.parser = [RSFeedParser sharedInstance];
        instance.transportLayer = [self createSimpleOperationManager];
    });
    return instance;
}

+ (AFHTTPRequestOperationManager *)createSimpleOperationManager {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [[AFXMLParserResponseSerializer alloc] init];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/xml", @"text/xml",@"application/rss+xml"]];
    return manager;
}

- (void)feedFromStringUrl:(NSString *)url success:(RSItemsBlock)success failure:(RSErrorBlock)failure {
    @weakify(self);
    [self.transportLayer GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        @strongify(self);
        NSDictionary *dom = [NSDictionary dictionaryWithXMLParser:responseObject];
        NSArray *items = [self.parser itemFeed:dom];
        success(items);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

@end
