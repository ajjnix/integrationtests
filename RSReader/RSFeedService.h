//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RSFeedService : NSObject

+ (instancetype)sharedInstance;
- (void)feedFromStringUrl:(NSString *)url success:(RSItemsBlock)success failure:(RSErrorBlock)failure;

@end
