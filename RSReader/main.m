//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSAppDelegate.h"
#import "RSTestingAppDelegate.h"


int main(int argc, char * argv[]) {
    @autoreleasepool {
        Class appDelegateClass = (NSClassFromString(@"XCTestCase") ? [RSTestingAppDelegate class] : [RSAppDelegate class]);
        return UIApplicationMain(argc, argv, nil, NSStringFromClass(appDelegateClass));
    }
}
