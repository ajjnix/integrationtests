//
//  Created by ajjnix on 13/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSFeedItem.h"


@interface RSFeedItem ()

@property (copy, nonatomic, readwrite) NSString *title;
@property (copy, nonatomic, readwrite) NSString *descriptionNews;
@property (strong, nonatomic, readwrite) NSDate *pubDate;
@property (strong, nonatomic, readwrite) NSURL *link;

@end


@implementation RSFeedItem

+ (instancetype)initWithTitle:(NSString *)title descriptionNews:(NSString *)descriptionNews pubDate:(NSDate *)pubDate link:(NSURL *)link {
    return [[self alloc] initWithTitle:title descriptionNews:descriptionNews pubDate:pubDate link:link];
}

- (instancetype)initWithTitle:(NSString *)title descriptionNews:(NSString *)descriptionNews pubDate:(NSDate *)pubDate link:(NSURL *)link {
    self = [super init];
    if (self != nil) {
        self.title = title;
        self.descriptionNews = descriptionNews;
        self.pubDate = pubDate;
        self.link = link;
    }
    return self;
}

@end
