//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
