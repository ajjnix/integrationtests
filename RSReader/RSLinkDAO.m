//
//  Created by ajjnix on 20/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSLinkDAO.h"
#import "RSLinkEntity.h"
#import <MagicalRecord/MagicalRecord.h>
#import "NSString+RS_RSS.h"


@interface RSLinkDAO ()
@end


@implementation RSLinkDAO

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static RSLinkDAO *instance;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)add:(NSString *)link {
    NSString *url = [link convertToBaseHttp];
    RSLinkEntity *entity = [self linkToLinkEntity:url];
    [entity.managedObjectContext MR_saveToPersistentStoreAndWait];
}

- (NSArray *)list {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (![standardUserDefaults boolForKey:RSHasBeenAddStandardLink]) {
        [self addStandartLink];
        [standardUserDefaults setBool:YES forKey:RSHasBeenAddStandardLink];
        [standardUserDefaults synchronize];
    }
    
    NSArray *all = [RSLinkEntity MR_findAll];
    return [self linkEntityToLink:all];
}

- (void)addStandartLink {
    RSLinkEntity *entity = [self linkToLinkEntity:@"http://developer.apple.com/news/rss/news.rss"];
    [entity.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    RSLinkEntity *entity1 = [self linkToLinkEntity:@"http://news.rambler.ru/rss/world"];
    [entity1.managedObjectContext MR_saveToPersistentStoreAndWait];
}

- (void)remove:(NSString *)link {
    RSLinkEntity *entity = [self entityWithLink:link];
    [entity MR_deleteEntity];
    [entity.managedObjectContext MR_saveToPersistentStoreAndWait];
}


#pragma mark - convert

- (NSArray *)linkEntityToLink:(NSArray *)entitys {
    return [entitys bk_map:^id(RSLinkEntity *entity) {
        return entity.link;
    }];
}

- (RSLinkEntity *)linkToLinkEntity:(NSString *)link {
    RSLinkEntity *entity = [RSLinkEntity MR_createEntity];
    entity.link = link;
    return entity;
}

- (RSLinkEntity *)entityWithLink:(NSString *)link {
    return [RSLinkEntity MR_findFirstByAttribute:@"link" withValue:link];
}

@end
