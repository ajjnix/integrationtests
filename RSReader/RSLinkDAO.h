//
//  Created by ajjnix on 20/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RSLinkDAO : NSObject

+ (instancetype)sharedInstance;

- (void)add:(NSString *)link;
- (NSArray *)list;
- (void)remove:(NSString *)link;

@end
