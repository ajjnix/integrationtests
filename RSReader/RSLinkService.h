//
//  Created by ajjnix on 20/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RSLinkService : NSObject

+ (instancetype)sharedInstance;

- (void)add:(NSString *)link success:(RSEmptyBlock)success failure:(RSErrorBlock)failure;
- (void)list:(RSItemsBlock)callback failure:(RSEmptyBlock)failure;
- (void)remove:(NSString *)link success:(RSEmptyBlock)success failure:(RSErrorBlock)failure;

@end
