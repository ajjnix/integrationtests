//
//  Created by ajjnix on 12/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSFeedParser.h"
#import <MWFeedParser/NSDate+InternetDateTime.h>
#import "RSFeedItem.h"


NSString * const RSFeedParserChannel = @"channel";
NSString * const RSFeedParserItem = @"item";
NSString * const RSFeedParserTitle = @"title";
NSString * const RSFeedParserPubDate = @"pubDate";
NSString * const RSFeedParserDescription = @"description";
NSString * const RSFeedParserLink = @"link";


@implementation RSFeedParser

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static RSFeedParser *instance;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSArray *)itemFeed:(NSDictionary *)dom {
    NSDictionary *channel = dom[RSFeedParserChannel];
    NSArray *items = channel[RSFeedParserItem];
    return [items bk_map:^id(NSDictionary *item) {
        NSString *title = item[RSFeedParserTitle];
        NSString *description = item[RSFeedParserDescription];
        NSString *pubDateString = item[RSFeedParserPubDate];
        NSString *linkString = item[RSFeedParserLink];
        
        NSDate *pubDate = [NSDate dateFromInternetDateTimeString:pubDateString formatHint:DateFormatHintRFC822];
        NSURL *link = [NSURL URLWithString:linkString];
        return [RSFeedItem initWithTitle:title descriptionNews:description pubDate:pubDate link:link];
    }];
}


@end
