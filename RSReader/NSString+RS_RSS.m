//
//  Created by ajjnix on 20/12/15.
//  Copyright © 2015 ajjnix. All rights reserved.
//

#import "NSString+RS_RSS.h"


@implementation NSString (RS_RSS)

- (instancetype)convertToBaseHttp {
    NSRange rangeHttp = [self rangeOfString:@"http://"];
    NSRange rangeHttps = [self rangeOfString:@"https://"];
    if (rangeHttp.location != NSNotFound || rangeHttps.location != NSNotFound) {
        return self;
    }
    
    return [NSString stringWithFormat:@"http://%@", self];
}

@end
