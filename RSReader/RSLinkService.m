//
//  Created by ajjnix on 20/12/15.
//  Copyright (c) 2015 ajjnix. All rights reserved.
//

#import "RSLinkService.h"
#import "RSLinkDAO.h"


@interface RSLinkService ()

@property (strong, nonatomic) RSLinkDAO *dao;

@end


@implementation RSLinkService

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static RSLinkService *instance;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.dao = [RSLinkDAO sharedInstance];
    });
    return instance;
}

- (void)add:(NSString *)link success:(RSEmptyBlock)success failure:(RSErrorBlock)failure {
    [self.dao add:link];
    success();
}

- (void)list:(RSItemsBlock)callback failure:(RSEmptyBlock)failure {
    NSArray *list = [self.dao list];
    callback(list);
}

- (void)remove:(NSString *)link success:(RSEmptyBlock)success failure:(RSErrorBlock)failure {
    [self.dao remove:link];
    success();
}

@end
